// Création de mon api audio pour analyser les basses
var audioApi = function(player) {
    var self = this;
    var analyser;

    // creation de mon contexte audio
    var audioCtx = new (window.AudioContext || window.webkitAudioContext);

    // creation de mon analyseur qui etudira les basses
    analyser = audioCtx.createAnalyser();
    analyser.fftSize = 256;
    player.crossOrigin = 'anonymous';

    // recuperation de la source audio present dans le HTML
    var source = audioCtx.createMediaElementSource(player);

    // creation de mon filtre passe bas qui me servira a etudier uniquement les basses fréquences
    var biquadFilter = audioCtx.createBiquadFilter();

    // connections du filtre a la source et de l'analyseur au filtre
    source.connect(biquadFilter);
    biquadFilter.connect(analyser);
    source.connect(audioCtx.destination);


    // parametrage du filtre pour qu'il n'ecoute que les fréquences inférieurs a 50Hz
    biquadFilter.type = "lowpass";
    biquadFilter.frequency.value = 50;
    biquadFilter.gain.value = 10;

    // proprietes initiales et initialisations
    this.volume = 0;
    this.streamData = new Uint8Array(128);

    var sampleAudioStream = function() {

        // Recuperation de toutes les valeurs a l'instant t pour toutes les fréquences
        analyser.getByteFrequencyData(self.streamData);

        // calcul du volume total maximum
        var gM = 0;

        // Pour chaque valeure de fréquence du tableau de donnée qui ont une valeure de gain supérieur a 5 j'incremente ma valeur gM

        for (var i = 0; i < self.streamData.length ; i++) {

            if(self.streamData[i] > 5){

                gM += self.streamData[i] ;


            }
        }

        // Je renvoie ma valeur volume

        self.volume = gM;
    };

    // Je repete mon etude de données toutes les 20ms

    setInterval(sampleAudioStream, 20);
}