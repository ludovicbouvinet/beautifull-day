class Television extends PIXI.Container {

    constructor(opts) {

        super();

        // Creation de la télévision avec son fond
        var background = PIXI.Texture.fromImage('./images/bg.png');
        this.tilingSprite = new PIXI.extras.TilingSprite(background, 500, 360);

        // Mise en mémoire des dimenssions de la télé
        var recBackground = new PIXI.Rectangle();
        this.tilingSprite.getBounds(true, recBackground);


        // this.y = renderer.height - recBackground.height;

        // setting des positions de la tele
        this.y = opts.position.y;
        this.x = opts.position.x;

        // Creation de l'ecran noir derriere mes images
        var blackGraphic = new PIXI.Graphics();

        blackGraphic.beginFill(0x000000);

        blackGraphic.drawRect(0, 0, 400, 340);

        this.addChild(blackGraphic);


        // Nombre de videos que j'ai en stock
        this.nbVideos = 9;

        // Tableau qui stocke toutes mes videos
        this.videos = [];

        this.videosObj = [];

        // Creation de toutes les sprites vidéos
        for(var z = 0; z < this.nbVideos; z ++){

            // Creation de ma variable numero video pour catcher mes videos dans le dossier
            var nbVideo = z + 1;

            // Creation de l'url par rapport a variable nbVideo
            var videoURI = './videos/V'+ nbVideo +'.mp4';

            // Je creer un element DOM video
            var video = document.createElement('video');

            // Je lui set ses parametres URL, son loop a true et je coupe son son puis je met ma video dans un tableau qui stocke toutes les videos
            video.src = videoURI;
            video.loop = true;
            video.muted = true;
            this.videosObj.push(video);

            // Je creer ensuite une texture avec ma video
            var videoTexture = PIXI.Texture.fromVideo(video);

            // Je creer une sprite a partir de ma texture
            var videoSprite = new PIXI.Sprite(videoTexture);

            // Je desactive l'autoplay de ma texture
            videoSprite.texture.baseTexture.autoPlay = false;

            // Je met ensuite ma texture dans mon tableau qui stocke toutes les textures videos
            this.videos.push(videoSprite);

        }


        // Je recupere le nombre de video de glitch que j'ai
        this.nbGlitchs = opts.nbGlitchs;

        // Je tire un numero de video glitch au hasard
        var nbGlitch = Math.floor(Math.random() * this.nbGlitchs) + 1;

        // Je definit mon URL de video glitch en fonction de mon numero de glitch
        var videoURI = './videos/G'+ nbGlitch +'.mp4';

        // Je creer un element DOM avec une video
        this.glitch = document.createElement('video');

        // Je lui set ses parametre comme son URL et sa valeure de loop a true
        this.glitch.src = videoURI;
        this.glitch.loop = true;

        // Creation d'une texture a partir de la video de glitch
        var glitchTexture = PIXI.Texture.fromVideo( this.glitch);

        // Creation d'un spritre a partir de cette texture
        var glitchSprite = new PIXI.Sprite(glitchTexture);

        // Setting de son opacité reduite de ma sprite de glitch
        glitchSprite.alpha = 0.54;

        // Je set mmon sprite de glitch comme etant ma video actuelle
        this.videoSprite = glitchSprite;

        // Je set les valeurs de dimmensions et de positions a ma video playable dans l'ecran
        this.videoSprite.width = 380;
        this.videoSprite.height = 305;

        this.videoSprite.y = 20;
        this.videoSprite.x = 15;

        // J'ajoute la video au container pour qu'elle soit rendu
        this.addChild(this.videoSprite);

        // Ainsi que le 'background' de la tele que je fais passer par dessus pour que l'image soit bien dans l'encart creuser
        this.addChild(this.tilingSprite);

        // J'autorise les changements de videos
        this.change = true;

        // Je specifie que le glitch est toujours actif
        this.glitchApply = true;


    }

    update(basse, filter){


        // si le volume de basse est superieur a 100 et que le changement est possible
        if(basse.volume > 1100 && this.change == true){

            // Si la video de glitch et toujours actif
            if(this.glitchApply == true){

                // Je met en pause la video de glitch et je specifie que mon glitch n'est plus actif
                this.glitch.pause();
                this.glitchApply = false;

            }
            else{
                // Sinon je met en pause la video qui est play actuellement pour eviter que toutes les videos se jouent meme si elles ne sont plus a l'ecran.
                this.videosObj[this.numVideo].pause();

            }
            do {
                // Je tire un numero de video au hasard parmis tout les videos que j'ai en stock
                this.numVideo = Math.floor(Math.random() * this.nbVideos);
                // Tant que la video que je selectionne est egale a la video actuellement play
            } while (this.videoSprite == this.videos[this.numVideo]);

            // Je supprime l'ancienne video actif de mon container
            this.removeChild(this.videoSprite);

            // Je set ma nouvelle video dans mon container a partir du numero tiré et de la video correspondante dans le tableau
            this.videoSprite = this.videos[this.numVideo];

            // Je remet a zero le player de ma texture pour que la video soit relancé de zero
            this.videoSprite.texture.baseTexture.source.currentTime = 0;

            // J'applique mon filtre sur la video et je lui set ses parametres de positions et de dimenssions
            this.videoSprite.filters = [filter];
            this.videoSprite.width = 380;
            this.videoSprite.height = 305;

            this.videoSprite.y = 20;
            this.videoSprite.x = 15;

            // Je play ma nouvelle video dans le container
            this.videosObj[this.numVideo].play();


            // Je rajoute cette video dans le container
            this.addChild(this.videoSprite);

            // Puis je supprime et remet le 'background' de ma tele pour qu'elle soit tjr au premier plan
            this.removeChild(this.tilingSprite);

            this.addChild(this.tilingSprite);

            // J'empeche les changements de videos pour le moment
            this.change = false;

        }

        // Si mon volume de basse passe sous la barre des 850 et que je n'ai toujours pas l'autorisation de changer de videos
        if(basse.volume < 850 && this.change == false){
            // Je lui redonne l'autorisation de changer au prochain pic de basse
            this.change = true;

        }
    }

}