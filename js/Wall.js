class Wall extends PIXI.Container {

    constructor() {

        super();
        
        // creation de mon muret en texture d'asphalt
        var asphalt = PIXI.Texture.fromImage('./images/asphalt.jpg');
        this.tilingSprite = new PIXI.extras.TilingSprite(asphalt, renderer.width, 146);

        // Mise en mémoire de la taille de mon muret
        var recAsphalt = new PIXI.Rectangle();
        this.tilingSprite.getBounds(true, recAsphalt);

        // Positionnement en bas de mon canvas du muret
        this.y = renderer.height - recAsphalt.height;

        // Ajout du muret a mon container
        this.addChild(this.tilingSprite);


    }

    update(){
    }

}