// Mise en mémoire de ma piste audio present dans le DOM
var audio = document.querySelector('#player');

// creation de mon API audio avec la piste audio du DOM
var audioInfo = new audioApi(audio);

// Mise en place du renderer aux dimenssions de ma fenetre
renderer = new PIXI.WebGLRenderer(window.innerWidth, window.innerHeight, {transparent: true});

//Ajout du canvas au HTML
document.querySelector('.cadre').appendChild(renderer.view);

//Creation de mon container stage qui sera rendu a chaque frame
var stage = new PIXI.Container();

//Rendu du 'stage'
renderer.render(stage);

// creation d'un shader pour modifier les images de mes televisions
function CustomFilter(fragmentSource)
{

    data = PIXI.Filter.call(this,
        // vertex shader
        null,
        // fragment shader
        fragmentSource,
        {
            "tDiffuse": 	{ type: "t", value: null },
            "time": 		{ type: "f", value: 0.0 },
            "distortion":   { type: "f", value: 0. },
            "distortion2":  { type: "f", value: 2. },
            "speed":     	{ type: "f", value: 1 },
            "rollSpeed":    { type: "f", value: 0. },
        }
    );
}

CustomFilter.prototype = Object.create(PIXI.Filter.prototype);
CustomFilter.prototype.constructor = CustomFilter;

// Tableau contenant toutes mes télévisions
var televisions = [];

// Nombre de tv que je veux generer
var nbTelevisions = 3; // 7

// Mise en place de ma position de démarage de ma premiere ligne en prenant en compte la taille du canvas et du muret
var y = renderer.height - 360 - 150;

// Mise en mémoire de la largeur de la ligne
var widthLine = renderer.width;

// Nombre de places réstantes sur la ligne
var placesEmpties = Math.floor(widthLine / 500);

// pixels en superflus restants
var offset = renderer.width - (placesEmpties * 500);

// Mise en mémoire de la derniere position en X de la derniere télevision avec sa largeur permettant de savoir a partir de quelle valeure de x je peux poser une nnouvelle tele
var memoryPosition = 0;

// Tableau qui permet de se souvenir de toutes les valeurs de x des télévisions de la ligne permettant de definir l'intervalle de positions de x des télé de la ligne supérieur
var line = [];

// Valeur du premier offset de la ligne
var firstOffset = 0;


// POur chaque television que nous allons creer
for(var i = 0; i < nbTelevisions; i ++){

    // Si il reste de la place sur la ligne
    if(placesEmpties != 0){

        // Je tire un offset au hasard
        var partOffset = Math.random() * offset;
        // Je set sa veleur de x en fonction de la memory position et de la partie d'offset a rajouter
        var x = memoryPosition + (partOffset);

        // J'enleve une place sur la ligne
        placesEmpties -= 1;

        // J'enleve la partie d'offset que je viens d'utiliser du offset total que je possedais
        offset -= (partOffset);

        // Je set ma nouvelle memoryPosition
        memoryPosition = x + 500;

        // Je memorise la derniere valeure de x
        line.push(x);
    }

    //Si il ne reste plus de place sur la ligne et qu'il reste assez de place sur la hauteur pour pouvoir rajouter des télé au dessus
    else if( y - 360 > 0){

        // Je remonte d'une ligne
        y -= 360;

        // Je redefini mon partOffset a zero
        partOffset = 0;

        // Je recalcule ma largeur de ligne en prenant compte de la largeur définit par la premiere tele et la derniere tele pour qu'uaucune tele ne flotte dans le vide
        widthLine = (line[line.length-1] + 500) - line[0];

        // Je recalcue le nombre de tele qui peuvent rentrer dans cette largeur de ligne
        placesEmpties = Math.floor(widthLine / 500);


        offset = widthLine - (placesEmpties * 500);
        memoryPosition = 0;

        // Je redefinie mon premier offset en fonction du offset insinuer par la premiere tele de la ligne précédente
        firstOffset = line[0];

        // Je remet mon tableau de position a zero de la ligne
        line = [];

        // meme logique que precedement
        var partOffset = Math.random() * offset;
        var x = memoryPosition + (partOffset) + firstOffset;
        placesEmpties -= 1;
        offset -= (partOffset);
        memoryPosition = x + 500;
        line.push(x);
    }


    // Nombre de videos de glitchs présente dans la base de donnée
    this.nbGlitchs = 5;

    // Options a transmettre pour la creation de ma tele comme sa position et le nombre de video glitch en base de données
    var opts = {
        'position': {
            x : x,
            y: y
        },
        'nbGlitchs': this.nbGlitchs
    };


    // Creation d'une nouvelle television
    var television = new Television(opts);

    // Ajout de cette television dans le tableau qui se souvient de toutes les télévisions
    televisions[i] = television;

    // Ajout de la television au stage
    stage.addChild(television);

}


// Rajout de mon cadre final autour de ma scene
var cadre = new Cadre();

// Ajout de ce cadre
stage.addChild(cadre);

// Load du shader permettant de tricker l'image de mes télés
PIXI.loader.add('shader','js/tv.frag');

// Une fois le chargement complet, lancement de la fonction onLoader
PIXI.loader.once('complete',onLoaded);

PIXI.loader.load();

//Creation de ma variable qui va stocker mon filtre de shader
var filter;


function onLoaded (loader,res) {

    // Mise en memoire des données qu'on veut partager au shader pour les parametres a changer
    var fragmentSrc = res.shader.data;

    //Creation de mon shader avec les parametres
    filter = new CustomFilter(fragmentSrc);

    // Mise a jour a chaque frame avec le filtre creer
    update(filter);

}
function update(filter){

    // pour chaque televisions creer on applique un filtre avec de nouveaux parametres
    for(var i = 0; i < televisions.length; i ++){

        // Changement des parametres des filtres
        filter.uniforms.time = Math.random() * 20;
        filter.uniforms.distortion2 = 2 + Math.exp((audioInfo.volume * 18) / 6000);
        filter.uniforms.rollSpeed = Math.floor((Math.exp(audioInfo.volume / 1600)) -1);

        // Mise a jour des televisions
        televisions[i].update(audioInfo, filter);
    }


    // Rendu de la scene
    renderer.render(stage);

    // Mise en boucle de la fonction de mise a jour
    requestAnimationFrame(function(){
        update(filter);
    });
}