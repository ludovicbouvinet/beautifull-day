class Cadre extends PIXI.Container {

    constructor() {

        super();

        // Je creer ma texture de vitre sale par dessus ma vitrine
        this.frontGlass = PIXI.Texture.fromImage('./Images/glass.jpg');

        
        // Je definit la taille de ma fenetre et son opacité
        this.tilingSpriteFrontGlasse = new PIXI.extras.TilingSprite(this.frontGlass, renderer.width, renderer.height);
        this.tilingSpriteFrontGlasse.alpha = 0.1;

        // J'ajoute la vitre a ma scéne
        this.addChild(this.tilingSpriteFrontGlasse);

        // Je creer ensuite une armature en creant un rectangle transparant avec une bordure 
        var armature = new PIXI.Graphics();

        armature.beginFill(0x000000, 0);

        armature.lineStyle(5, 0x353535);

        armature.drawRect(3, 2, renderer.width - (5), renderer.height - 150);
        
        // J'ajoute l'armature a la scene

        this.addChild(armature);
        
        // Je creer le petit muret present sous la vitre

        var wall = new Wall();
        
        // Je rajoute mon mur a la scene 
        
        this.addChild(wall);




    }

    update(){
    }

}